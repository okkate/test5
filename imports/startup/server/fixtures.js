// Fill the DB with example data on startup

import { Meteor } from 'meteor/meteor';
import {_} from "meteor/underscore";
import {Accounts} from "meteor/accounts-base";


Meteor.startup(() => {
  // if the Links collection is empty
  var users = [
    {name: 'Admin User', email: 'admin@admin', roles: ['admin']},
    {name: 'Classic User', email: 'classic@classic', roles: ['classic']},
    {name: 'Privileged User', email: 'privileged@privileged', roles: ['privileged']}
  ];
  _.each(users, function (user) {
    if (Meteor.users.findOne({'emails.address': user.email})) {
      return;
    }
    var id;
    id = Accounts.createUser({
      email: user.email,
      password: "1111",
      profile: {name: user.name}
    });
    if (user.roles.length > 0) {
      // Need _id of existing user record so this call must come
      // after `Accounts.createUser` or `Accounts.onCreate`
      // Добавление в группу webapp
      Roles.addUsersToRoles(id, user.roles, 'webapp');
    }

  });

});
