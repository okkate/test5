import {Meteor} from "meteor/meteor"
import SimpleSchema from "simpl-schema"

import {Accounts} from "../both/accounts-collection.js"

// ***************************************************************
// PUBLICATIONS (For the documents collection)
// ***************************************************************

// DOCUMENTS INDEX
// -------------------------------------------------------
// accounts.chats.list

Meteor.publish("accounts.chats.list", function accountsAll(business_id, category_id, limit, type, query) {
  if (type === 3) {
    // var base = 10;
    // var normalizedLimit = limit + (base - (limit % base))
    let cur_query = [{'business_id': business_id}, {$nor: [{'conversation.status': 'active'}, {'conversation.status': 'open'}]}]
    if (!_.isUndefined(category_id)) {
      if (category_id === 'common') {
        cur_query.push({category: {$exists: false}})
      } else if (category_id === 'all') {
      } else {
        cur_query.push({category: category_id})
      }
    } else {
      cur_query.push({category: {$exists: false}})
    }
    return Accounts.find({$and: cur_query}, {sort: {updatedAt: -1}, limit: limit,})
  } else if (type === 2) {
    let user = Meteor.userId()
    let cur_query = [{'business_id': business_id},
      {'added_managers': {$ne: user}},
      {'assigned_manager': {$ne: user}},
      {'owner_manager': {$ne: user}},
      {'conversation.status': 'active'}];
    if (!_.isUndefined(category_id)) {
      if (category_id === 'common') {
        cur_query.push({category: {$exists: false}})
      } else if (category_id === 'all') {
      } else {
        cur_query.push({category: category_id})
      }
    } else {
      cur_query.push({category: {$exists: false}})
    }
    return Accounts.find({$and: cur_query}, {sort: {updatedAt: -1}, limit: limit,})
  } else if (type === 0) {
    let cur_query = [{'business_id': business_id}, {'conversation.status': 'open'}];
    if (!_.isUndefined(category_id)) {
      if (category_id === 'common') {
        cur_query.push({category: {$exists: false}})
      } else if (category_id === 'all') {
      } else {
        cur_query.push({category: category_id})
      }
    } else {
      cur_query.push({category: {$exists: false}})
    }
    return Accounts.find({$and: cur_query}, {sort: {updatedAt: -1}, limit: limit})
  } else if (type === 4) {
    let cur_query = query
    if (!_.isEmpty(query)) {
      if (!_.isUndefined(category_id)) {
        if (category_id === 'common') {
          cur_query.push({category: {$exists: false}})
        } else if (category_id === 'all') {
        } else {
          cur_query.push({category: category_id})
        }
      } else {
        cur_query.push({category: {$exists: false}})
      }
      return Accounts.find({$and: cur_query}, {sort: {createdAt: -1}})
    } else {
      let cur_query = [{business_id: business_id}];
      if (!_.isUndefined(category_id)) {
        if (category_id === 'common') {
          cur_query.push({category: {$exists: false}})
        } else if (category_id === 'all') {
        } else {
          cur_query.push({category: category_id})
        }
      } else {
        cur_query.push({category: {$exists: false}})
      }
      Accounts.find({$and: cur_query}, {sort: {createdAt: -1}})
    }
  }
})

Meteor.publish('accounts.chats.user_owner', function accounts_user_owner(business_id, category_id, limit) {
  let cur_query = [
    {'business_id': business_id},
    {'owner_manager': Meteor.userId()},
    {'conversation.status': 'active'}
  ];
  if (!_.isUndefined(category_id)) {
    if (category_id === 'common') {
      cur_query.push({category: {$exists: false}})
    } else if (category_id === 'all') {
    } else {
      cur_query.push({category: category_id})
    }
  } else {
    cur_query.push({category: {$exists: false}})
  }
  return Accounts.find({
    $and: cur_query
  }, {sort: {updatedAt: -1}, limit: limit})
})
Meteor.publish('accounts.chats.user_added', function accounts_user_owner(business_id, category_id, limit) {
  let cur_query = [
    {'business_id': business_id},
    {'added_managers': Meteor.userId()},
    {'conversation.status': 'active'}
  ]
  if (!_.isUndefined(category_id)) {
    if (category_id === 'common') {
      cur_query.push({category: {$exists: false}})
    } else if (category_id === 'all') {
    } else {
      cur_query.push({category: category_id})
    }
  } else {
    cur_query.push({category: {$exists: false}})
  }
  return Accounts.find({
    $and: cur_query
  }, {sort: {updatedAt: -1}, limit: limit})
})
Meteor.publish('accounts.chats.user_assigned', function accounts_user_owner(business_id, category_id, limit) {
  let cur_query = [
    {'business_id': business_id},
    {'assigned_manager': Meteor.userId()},
    {'conversation.status': 'active'}
  ]
  if (!_.isUndefined(category_id)) {
    if (category_id === 'common') {
      cur_query.push({category: {$exists: false}})
    } else if (category_id === 'all') {
    } else {
      cur_query.push({category: category_id})
    }
  } else {
    cur_query.push({category: {$exists: false}})
  }
  return Accounts.find({
    $and: cur_query
  }, {sort: {updatedAt: -1}, limit: limit})
})

Meteor.publish("accounts.business.list.withoutconversations", function accountsAll(business_id, limit = undefined) {
  if (limit){
    return Accounts.find({business_id: business_id}, {
      fields: {
        conversation: 0
      },
      sort: {updatedAt: 1},
      limit: limit
    })
  } else {
    return Accounts.find({business_id: business_id}, {
      fields: {
        conversation: 0
      },
      sort: {updatedAt: 1}
    })
  }
})


Meteor.publish("accounts.user", function chatsAllBusiness(_id) {
  return Accounts.find({_id: _id})
})

Meteor.publish("accounts.message_chains", function findBusinessId(business_id) {
  return Accounts.find({business_id: business_id},{fields: {message_chains_id: 1}})
})
