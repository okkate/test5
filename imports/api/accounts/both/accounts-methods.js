import {Meteor} from "meteor/meteor"
import SimpleSchema from "simpl-schema"

import {Accounts} from "./accounts-collection.js"
import AccountsSchema from "./schemas/accounts-schema.js"


// ***************************************************************
// METHODS (related to the documents collection)
// ***************************************************************

Meteor.methods({

  createAccount: function (account) {
    return Accounts.insert({
      user_id: account.user_id,
      name:account.name,
      mail:account.mail,
      updatedAt: new Date()
    })
  }
})




