import { Mongo } from "meteor/mongo"

import AccountsSchema from "./schemas/accounts-schema"

// ***************************************************************
// DOCUMENTS Collection
// ***************************************************************

export const Accounts = new Mongo.Collection("accounts")

// We use explicit methods, so deny everything
Accounts.allow({
  insert() {
    return false
  },
  update() {
    return false
  },
  remove() {
    return false
  }
})

Accounts.deny({
  insert() {
    return true
  },
  update() {
    return true
  },
  remove() {
    return true
  }
})

// Must remember to attach the schema to the collection
Accounts.attachSchema(AccountsSchema)
