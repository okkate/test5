import SimpleSchema from "simpl-schema"

// ***************************************************************
// Document schema
// ***************************************************************

const AccountsSchema = new SimpleSchema({
  user_id: {
    type: String,
    unique: true,
    index: true
  },
  createdAt: {
    type: Date,
    optional: true,
    denyUpdate: true,
  },
  updatedAt: {
    type: Date,
    optional: true,
  }
})

export default AccountsSchema
