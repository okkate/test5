import './home.html';

import '../../components/upload/upload.js';
import '../../components/info/info.js';
import Images from '/imports/UploadImage'
import {Meteor} from "meteor/meteor";


Template.App_home.helpers({
    files() {
        return Images.find()
    },
    admin() {
        let loggedInUser = Meteor.users.findOne(Meteor.userId());
        if (Roles.userIsInRole(loggedInUser, ['admin'], 'webapp')) {
            return true
        }
        return false

    }
})