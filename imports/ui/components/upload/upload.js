import './upload.html';
import {Template} from 'meteor/templating';
import {ReactiveVar} from 'meteor/reactive-var';
import Images from '/imports/UploadImage'
import {Meteor} from "meteor/meteor";
import {Roles} from 'meteor/alanning:roles';

Template.upload.onCreated(function () {
    this.currentUpload = new ReactiveVar(false);
});

Template.upload.helpers({
    currentUpload() {
        return Template.instance().currentUpload.get();
    }
});

Template.upload.events({
    'change #fileInput'(e, template) {
        let loggedInUser = Meteor.users.findOne(this.userId);
        let type = e.currentTarget.files[0].type

        if (type != 'image/png' && type != 'application/zip' &&
            type != 'tiff' && type != 'pdf' && type != 'image/jpg') {
            return alert("Вы не можете загрузить файл в этом формате")
        }

        if (Roles.userIsInRole(loggedInUser, ['classic'], 'webapp')
            && e.currentTarget.files[i].size >= 500000) {
            return alert("Вы не можете загрузить размером файл больше 5МБ")
        } else {
            if (e.currentTarget.files && e.currentTarget.files[0]) {
                const upload = Images.insert({
                    file: e.currentTarget.files[0],
                    streams: 'dynamic',
                    chunkSize: 'dynamic',
                    meta: {
                        date: new Date()
                    }
                }, false);

                upload.on('start', function () {
                    template.currentUpload.set(this);
                });

                upload.on('end', function (error, fileObj) {
                    if (error) {
                        alert('Error during upload: ' + error);
                    } else {
                        alert('File "' + fileObj.name + '" successfully uploaded');
                    }
                    template.currentUpload.set(false);
                });

                upload.start();
            }
        }
    }
});